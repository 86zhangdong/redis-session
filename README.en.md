# redis-session

#### Description
tomcat redis session

#### Instructions

  * JedisStore: copy redis-session-manager.jar+jedis-2.9.0.jar+commons-pool2-2.4.2.jar to tomcat/lib, vi server.xml and add Manager+Store

>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.PersistentManager">
    	<Store className="org.apache.catalina.session.JedisStore" 
    		host="localhost" port="6379" database="0" timeout="2000" prefix="sid:" maxInactiveInterval="1800"
			maxTotal="8" maxIdle="4" minIdle="2" lifo="true"
    	/>
    </Manager>
    </Context>

  * RedisSessionManager: copy redis-session-manager.jar+jedis-2.9.0.jar+commons-pool2-2.4.2.jar to tomcat/lib, vi server.xml and add Manager

>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.RedisSessionManager" maxTotal="4" maxIdle="2" minIdle="1" prefix="sid:"/>
    </Context>

  * RedisSessionFilter: import com.xlongwei:redis-session:0.0.1, vi web.xml and add filter, optional vi server.xml and add NoSessionManager

>
	#WEB-INF/web.xml cookie: sessionCookieName cookieDomain cookieContextPath cookieMaxAge
	<filter>
		<filter-name>redisSessionFilter</filter-name>
		<filter-class>com.xlongwei.session.RedisSessionFilter</filter-class>
		<init-param>
			<param-name>prefix</param-name>
			<param-value>sid:</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>redisSessionFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.NoSessionManager" />
	</Context>
	
#### Configuration

>
	JedisPool: minIdle maxIdle maxTotal lifo fairness maxWaitMillis minEvictableIdleTimeMillis testOnCreate ... 
	Redis: host port password database timeout
	Sentinel: sentinelMaster=master hostAndPorts=host:port,host:port,host:port
	RedisCluster: hostAndPorts=host:port,host:port,host:port,host:port,host:port,host:port
	General: prefix maxInactiveInterval saveOnDirty(RedisSessionManager)
	cookie: sessionCookieName cookieDomain cookieContextPath cookieMaxAge ( RedisSessionFilter )
