# redis-session

#### 介绍
tomcat redis session

#### 使用说明

  * JedisStore：复制redis-session-manager.jar、jedis-2.9.0.jar、commons-pool2-2.4.2.jar到tomcat/lib目录，server.xml配置如下

>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.PersistentManager">
    	<Store className="org.apache.catalina.session.JedisStore" 
    		host="localhost" port="6379" database="0" timeout="2000" prefix="sid:" maxInactiveInterval="1800"
			maxTotal="8" maxIdle="4" minIdle="2" lifo="true"
    	/>
    </Manager>
    </Context>

  * RedisSessionManager：复制redis-session-manager.jar、jedis-2.9.0.jar、commons-pool2-2.4.2.jar到tomcat/lib目录，server.xml配置如下

>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.RedisSessionManager" maxTotal="4" maxIdle="2" minIdle="1" prefix="sid:"/>
    </Context>

  * RedisSessionFilter：引入依赖com.xlongwei:redis-session:0.0.1，web.xml配置filter，可选在server.xml配置NoSessionManager

>
	#WEB-INF/web.xml cookie配置：sessionCookieName、cookieDomain、cookieContextPath、cookieMaxAge
	<filter>
		<filter-name>redisSessionFilter</filter-name>
		<filter-class>com.xlongwei.session.RedisSessionFilter</filter-class>
		<init-param>
			<param-name>prefix</param-name>
			<param-value>sid:</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>redisSessionFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	#tomcat/conf/server.xml
	<Context docBase="" path="">
	<Manager className="org.apache.catalina.session.NoSessionManager" />
	</Context>
	
#### 配置参数

>
	JedisPool连接池：minIdle、maxIdle、maxTotal、lifo、fairness、maxWaitMillis、minEvictableIdleTimeMillis、testOnCreate等
	Redis单机：host主机、port端口、password密码、database数据库、timeout超时
	Sentinel哨兵：sentinelMaster+hostAndPorts
	RedisCluster集群：hostAndPorts=host:port,host:port,host:port,host:port,host:port,host:port
	Redis存储配置：prefix前缀、maxInactiveInterval过期、saveOnDirty（默认true有修改时保存，false即时保存，用于RedisSessionManager）
	cookie配置：sessionCookieName、cookieDomain、cookieContextPath、cookieMaxAge，仅用于RedisSessionFilter
