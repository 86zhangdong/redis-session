package org.apache.catalina.session;

import org.apache.catalina.Session;

/**
 * 使用RedisSessionFilter时，可选禁用tomcat默认的Manager
 */
public class NoSessionManager extends PersistentManagerBase {

	public Session createSession(String sessionId) {
		return null;
	}

}
